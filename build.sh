#!/bin/bash

# Lancement docker-compose
docker-compose up -d --build

# On pousse la configuration sur le Influx
docker exec influxDB_SDV /bin/bash "/opt/config.sh"

sleep 5
docker exec influxDB_SDV /usr/bin/telegraf --config /opt/telegraf/globalnet.conf >/dev/null 2>&1 &
